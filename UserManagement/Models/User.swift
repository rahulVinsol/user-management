//
//  User.swift
//  UserManagement
//
//  Created by Rahul Rawat on 27/05/21.
//

struct User {
    let firstName: String
    let lastName: String
    let age: Int
}
