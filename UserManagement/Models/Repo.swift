//
//  Repo.swift
//  UserManagement
//
//  Created by Rahul Rawat on 28/05/21.
//

import UIKit

protocol Repo {
    func getCount() -> Int
    
    subscript(index: Int) -> User { get }
    
    mutating func add(element: User)
    
    mutating func delete(index: Int)
    
    mutating func edit(index: Int, user: User)
}

class RepoImpl: Repo {
    private var array: [User] = [User]()

    func getCount() -> Int { array.count }
    
    subscript(index: Int) -> User { array[index] }
    
    func add(element: User) {
        array.append(element)
        NotificationCenter.default.post(name: Notification.Name(listChangedNotification), object: nil)
    }
    
    func delete(index: Int) {
        array.remove(at: index)
        NotificationCenter.default.post(name: Notification.Name(listChangedNotification), object: nil)
    }
    
    func edit(index: Int, user: User) {
        array[index] = user
        NotificationCenter.default.post(name: Notification.Name(listChangedNotification), object: nil)
    }
}
