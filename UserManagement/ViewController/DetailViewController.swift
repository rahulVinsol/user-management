//
//  DetailViewController.swift
//  UserManagement
//
//  Created by Rahul Rawat on 26/05/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    var user: User?
    var index: Int?
    
    private var repo: Repo = getRepo()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = user {
            updateUser(user: user)
        }
        
        let deleteButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteTapped))
        let editButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editTapped))
    
        self.navigationItem.rightBarButtonItems = [deleteButton, editButton]
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        repo.delete(index: index!)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func editTapped(_ sender: UIButton) {
        let editVc = CreateEditViewController(isCreationMode: false, user: user, index: index)
        editVc.delegate = self
        navigationController?.pushViewController(editVc, animated: true)
    }
}

extension DetailViewController: CreateEditViewControllerDelegate {
    func updateUser(user: User) {
        nameLabel.text = "\(user.firstName) \(user.lastName)"
        ageLabel.text = "\(user.age)"
    }
}
