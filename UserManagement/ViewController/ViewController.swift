//
//  ViewController.swift
//  UserManagement
//
//  Created by Rahul Rawat on 26/05/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(newUserAdded), name: Notification.Name(listChangedNotification), object: nil)
    }
    
    @objc func newUserAdded() {
        tableView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension ViewController: UITableViewDelegate {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailsSegue", let indexPath = tableView.indexPathForSelectedRow {
            let destination = segue.destination as! DetailViewController
            destination.user = getRepo()[indexPath.row]
            destination.index = indexPath.row
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        getRepo().getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let repo = getRepo()
        
        let user = repo[indexPath.row]
        
        cell.textLabel?.text = "\(user.firstName) \(user.lastName), \(user.age)"
        
        return cell
    }
}
