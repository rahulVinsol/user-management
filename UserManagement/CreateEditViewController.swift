//
//  CreateEditViewController.swift
//  UserManagement
//
//  Created by Rahul Rawat on 07/06/21.
//

import UIKit

protocol CreateEditViewControllerDelegate {
    func updateUser(user: User)
}

@IBDesignable class CreateEditViewController: UIViewController {

    var isCreationMode: Bool
    var user: User?
    var index: Int?
    var delegate: CreateEditViewControllerDelegate? = nil
    
    init(isCreationMode: Bool = true, user: User? = nil,index: Int? = nil) {
        self.isCreationMode = isCreationMode
        self.user = user
        self.index = index
        
        super.init(nibName: nil, bundle: nil)
        
        hidesBottomBarWhenPushed = !isCreationMode
    }
    
    required init?(coder: NSCoder) {
        self.isCreationMode = true
        self.user = nil
        self.index = nil
        
        super.init(coder: coder)
        
        hidesBottomBarWhenPushed = !isCreationMode
    }
    
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfSecondName: UITextField!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var btCreateUser: UIButton!
    
    override func loadView() {
        super.loadView()
        if let nib = Bundle.main.loadNibNamed("CreateEditViewController", owner: self),
           let nibView = nib.first as? UIView {
             view = nibView
        }
      }
    
    override func viewDidLoad() {
        btCreateUser.layer.cornerRadius = 10
        
        tfFirstName.delegate = self
        tfSecondName.delegate = self
        tfAge.delegate = self
        
        tfFirstName.becomeFirstResponder()
        
        if !isCreationMode {
            btCreateUser.setTitle("Update User", for: .normal)
            tfFirstName.text = user!.firstName
            tfSecondName.text = user!.lastName
            tfAge.text = String(user!.age)
        }
    }
    
    @IBAction func createUserTapped(_ sender: UIButton) {
        var errorOccured = false
        let errorColor = UIColor.gray.cgColor
        
        if tfFirstName.text?.isEmpty != false {
            tfFirstName.isError(baseColor: errorColor, numberOfShakes: 4, revert: false)
            errorOccured = true
        }
        
        if tfSecondName.text?.isEmpty != false {
            tfSecondName.isError(baseColor: errorColor, numberOfShakes: 4, revert: false)
            errorOccured = true
        }
        
        if tfAge.text?.isEmpty != false {
            tfAge.isError(baseColor: errorColor, numberOfShakes: 4, revert: false)
            errorOccured = true
        }
        
        if errorOccured {
            return
        }
        
        let user = User(firstName: tfFirstName.text!, lastName: tfSecondName.text!, age: Int(tfAge.text!)!)
        
        tfFirstName.text = ""
        tfSecondName.text = ""
        tfAge.text = ""
        
        tfFirstName.becomeFirstResponder()
        
        var repo = getRepo()
        
        if isCreationMode {
            repo.add(element: user)
            
            (tabBarController?.viewControllers?[0] as? UINavigationController)?.popToRootViewController(animated: false)
            tabBarController?.selectedIndex = 0
        } else {
            repo.edit(index: index!, user: user)
            
            delegate?.updateUser(user: user)
            navigationController?.popViewController(animated: true)
        }
    }
}

extension CreateEditViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField != tfAge {
            return true
        }
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return
            string.rangeOfCharacter(from: invalidCharacters) == nil
            && (textField.text?.isEmpty == false || string != "0")
            && Decimal(string: (textField.text! as NSString).replacingCharacters(in: range, with: string)) ?? 0 <= 100
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case tfFirstName:
            tfSecondName.becomeFirstResponder()
        case tfSecondName:
            tfAge.becomeFirstResponder()
        case tfAge:
            tfAge.resignFirstResponder()
            btCreateUser.sendActions(for: .touchUpInside)
        default:
            break
        }
        return true
    }
}

extension UITextField {
    func isError(baseColor: CGColor, numberOfShakes shakes: Float, revert: Bool) {
        let shake: CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = shakes
        shake.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(shake, forKey: "position")
    }
}
